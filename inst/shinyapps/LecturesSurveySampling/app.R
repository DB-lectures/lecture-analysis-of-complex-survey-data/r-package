require(shiny)
require(shinythemes)
require(reshape2)
require(ggplot2)
require(plyr)
require(dplyr)





ui <-function () {
  fluidPage(
    # Application title
    #setBackgroundColor("black"),
    navbarPage(
      "Analysis of Complex surveys",id="main_navbar",theme = shinytheme("slate"),
      tabPanel("Example1",
               fluidRow(
                 column(2,
                        h3("Population parameters"),
                        h4("Probabilities..."),
                        sliderInput('tab1.prob.f',p('...to be a woman'), 
                                    min=.2,max=.8,step=.01,value=.5),
                        column(2),column(5,p('Women')),column(5,p('Men')),
                        p('... to vote'),
                        column(2,''),
                        column(5,sliderInput( 'tab1.prob.f.vote','',min=0,max=1,step=.01,value=.8)),
                        column(5,sliderInput( 'tab1.prob.m.vote','',min=0,max=1,step=.01,value=.7)),
                        p('... to vote yes when voting'),
                        column(2),
                        column(5,sliderInput('tab1.prob.f.voteyes', '',min=0,max=1,step=.01,value=.6)),
                        column(5,sliderInput('tab1.prob.m.voteyes', '',min=0,max=1,step=.01,value=.4))),
                 
                 column(2,
                        h3('Sampling'),
                        h4('Sample size'),
                        sliderInput('tab1.samplesize','n', min=1,max=1000,step=10,value=1000),
                        h4('Inclusion probabilities'),
                        column(2),column(5,p('Women')),column(5,p('Men')),
                        column(2,'No'),
                        column(5,sliderInput('tab1.pifN','', min=0,max=1,step=.01,value=.5)),
                        column(5,sliderInput('tab1.pimN','', min=0,max=1,step=.01,value=.5)),
                        column(2,'Yes'),
                        column(5,sliderInput('tab1.pifY','', min=0,max=1,step=.01,value=.5)),
                        column(5,sliderInput('tab1.pimY','', min=0,max=1,step=.01,value=.5)),
                        column(2,'Abs.'),
                        column(5,sliderInput('tab1.pif0','', min=0,max=1,step=.01,value=.5)),
                        column(5,sliderInput('tab1.pim0',' ', min=0,max=1,step=.01,value=.5))),
                 column(2,
                        h3('Measurement error'),
                        h4('Women'),
                        column(3,'Truth'),column(9,'Answer'),
                        column(3,''),column(3,'No'),column(3,'Yes'),column(3,'Abs'),
                        column(3,'No'),
                        column(3,sliderInput('tab1.trans.wNN','', min=0,max=1,step=.01,value=.9)),
                        column(3,sliderInput('tab1.trans.wNY','', min=0,max=1,step=.01,value=.1)),
                        column(3,sliderInput('tab1.trans.wNA','', min=0,max=1,step=.01,value=0)),
                        column(3,'Yes'),
                        column(3,sliderInput('tab1.trans.wYN','', min=0,max=1,step=.01,value=0)),
                        column(3,sliderInput('tab1.trans.wYY','', min=0,max=1,step=.01,value=1)),
                        column(3,sliderInput('tab1.trans.wYA','', min=0,max=1,step=.01,value=0)),
                        column(3,'Abs.'),
                        column(3,sliderInput('tab1.trans.wAN','', min=0,max=1,step=.01,value=.2)),
                        column(3,sliderInput('tab1.trans.wAY','', min=0,max=1,step=.01,value=.8)),
                        column(3,sliderInput('tab1.trans.wAA','', min=0,max=1,step=.01,value=0)),
                        h4('Men'),
                        column(3,'Truth'),column(9,'Answer'),
                        column(3,''),column(3,'No'),column(3,'Yes'),column(3,'Abs'),
                        column(3,'No'),
                        column(3,sliderInput('tab1.trans.mNN','', min=0,max=1,step=.01,value=.9)),
                        column(3,sliderInput('tab1.trans.mNY','', min=0,max=1,step=.01,value=.1)),
                        column(3,sliderInput('tab1.trans.mNA','', min=0,max=1,step=.01,value=0)),
                        column(3,'Yes'),
                        column(3,sliderInput('tab1.trans.mYN','', min=0,max=1,step=.01,value=0)),
                        column(3,sliderInput('tab1.trans.mYY','', min=0,max=1,step=.01,value=1)),
                        column(3,sliderInput('tab1.trans.mYA','', min=0,max=1,step=.01,value=0)),
                        column(3,'Abs.'),
                        column(3,sliderInput('tab1.trans.mAN','', min=0,max=1,step=.01,value=.2)),
                        column(3,sliderInput('tab1.trans.mAY','', min=0,max=1,step=.01,value=.8)),
                        column(3,sliderInput('tab1.trans.mAA','', min=0,max=1,step=.01,value=0))),
                 column(4,
                        h3('Calibrating variables'),
                        h4('Bias (%)'),
                        column(6,sliderInput('tab1.censusbias.w','Women', min=-100,max=100,step=1,value=0)),
                        column(6,sliderInput('tab1.censusbias.m','Men', min=-100,max=100,step=.01,value=0)),
                        h4('Precision (%)'),
                        column(6,sliderInput('tab1.censusprec.w','Women', min=0,max=10,step=1,value=2)),
                        column(6,sliderInput('tab1.censusprec.m','Men', min=0,max=10,step=1,value=2)),
                        h4('Error correlation (%)'),
                        sliderInput('tab1.censuscorr','Women', min=-100,max=100,step=1,value=0))),
               fluidRow(
                 column(2,
                        h4("Generated population"),
                        plotOutput("tab1.plot1",height = 400)),
                 column(2,
                        h4("Generated sample"),
                        plotOutput("tab1.plot2",height = 400)),
                 column(2,
                        h4("Raw answers"),
                        plotOutput("tab1.plot3",height = 400)),
                 
                 column(2,
                        h4("Generated Census estimates"),
                        plotOutput("tab1.plot4",height = 400)),
                 
                 column(2,
                        h4("Generated Predictions"),
                        plotOutput("tab1.plot5",height = 400)),
                 column(2,
                        h4("Realisations versus Predictions"),
                        plotOutput("tab1.plot6",height = 400))
                 
               ))))}
server <- function(input , output,session) {
  library(dplyr)
  theme_dark2 = function() {
    theme_grey() %+replace%
      theme(
        # Specify axis options
        axis.line = element_blank(),  
        axis.text.x = element_text(color = "white",angle=90),  
        axis.text.y = element_text(colour = "white"),  
        axis.ticks = element_line(color = "white"),  
        axis.title.x = element_text(colour = "white"),  
        axis.title.y = element_text(colour = "white"),
        # Specify legend options
        legend.background = element_rect(color = NA, fill = " gray10"),  
        legend.key = element_rect(colour = "white",  fill = " gray10"),  
        legend.text = element_text(colour = "white"),  
        legend.title = element_text(colour = "white"),
        # Specify panel options
        panel.background = element_rect(fill = " gray10", color  =  NA),  
        panel.border = element_rect(fill = NA, color = "white"),  
        panel.grid.major = element_line(color = "grey35"),  
        panel.grid.minor = element_line(color = "grey20"),
        # Specify facetting options
        strip.background = element_rect(fill = "grey30", color = "grey10"),  
        strip.text.x = element_text(color = "white"),  
        strip.text.y = element_text(color = "white"),  
        # Specify plot options
        plot.background = element_rect(color = " gray10", fill = " gray10"),  
        plot.title = element_text(colour = "white"),  
        plot.subtitle = element_text(colour = "white"),  
        plot.caption = element_text( colour = "white")
      )}
  #ggplot(data=data.frame(x=1:15,z=1,y=factor(1:3)),aes(x=x,y=z,color=y))+geom_point()
  th<-theme_dark2()
  theme_set(th)
  my_palette <- c('lightblue', 'red', 'white','green')
  #names(my_palette)<-c('Original','Transformed',NA)
  #assign("scale_colour_discrete", function(..., values = my_palette) scale_colour_manual(..., values = values), globalenv())
  #assign("scale_fill_discrete", function(..., values = my_palette) scale_fill_manual(..., values = values), globalenv())
  #assign("scale_fill_ordinal", function(..., values = my_palette) scale_fill_manual(..., values = values), globalenv())
  #assign("scale_colour_ordinal", function(..., values = my_palette) scale_fill_manual(..., values = values), globalenv())
  #colScale <- scale_colour_manual(name = "Origin",values = c('Original'='lightblue', 'Transformed'='red','white'))
  #colScale2 <- scale_fill_manual(name = "Origin",values = c('Original'='lightblue', 'Transformed'='red','white'))
  
  #Risk with sampling with replacement approximation.
  
  
  
  tab1.Pop<-reactive({
    N0=rpois(n = 1,lambda = 1000000)
    
    
      Pop<-data.frame(Gender=c("Male","Female"),
                 Count=c(0,N0)+c(1,-1)*rbinom(n = 1,size=N0,prob = input$tab1.prob.f))
    
    
    probs=list("Female"=c(input$tab1.prob.f.vote*c(1-input$tab1.prob.f.voteyes,input$tab1.prob.f.voteyes),1-input$tab1.prob.f.vote),
               "Male"=c(input$tab1.prob.m.vote*c(1-input$tab1.prob.m.voteyes,input$tab1.prob.m.voteyes),1-input$tab1.prob.m.vote))
    
    Pop<-Pop|>
      plyr::ddply(~Gender,
                  function(d){
                    data.frame(y=c('No','Yes','Abs')|>factor(levels=c('No','Yes','Abs'))|>ordered(),Count=
                                 rmultinom(1,size=d$Count,prob=probs[[d$Gender]]))
                  })
    })
 
  tab1.Sample<-reactive({
    Pop<-tab1.Pop()
    pis<-Pop|>dplyr::mutate(pi=
                               c(input$tab1.pifN,
                                 input$tab1.pifY,
                                 input$tab1.pif0,
                                 input$tab1.pimN,
                                 input$tab1.pimY,
                                 input$tab1.pim0))
    
    Sample<-pis|>dplyr::mutate(sample=rmultinom(n=1,size = input$tab1.samplesize,prob =pis$Count*pis$pi ))|>
    dplyr::select(Gender,y,sample)
  })
  
  
  tab1.errorprobs<-reactive({
    errorprobs<-list(Male=list(No =c(No =input$tab1.trans.mNN,
                         Yes=input$tab1.trans.mNY,
                         Abs=input$tab1.trans.mNA),
                   Yes=c(No =input$tab1.trans.mYN,
                        Yes=input$tab1.trans.mYY,
                        Abs=input$tab1.trans.mYA),
                   Abs=c(No =input$tab1.trans.mAN,
                         Yes=input$tab1.trans.mAY,
                         Abs=input$tab1.trans.mAA)),
           Female=list(No =c(No =input$tab1.trans.wNN,
                             Yes=input$tab1.trans.wNY,
                             Abs=input$tab1.trans.wNA),
                       Yes=c(No =input$tab1.trans.wYN,
                             Yes=input$tab1.trans.wYY,
                             Abs=input$tab1.trans.wYA),
                       Abs=c(No =input$tab1.trans.wAN,
                             Yes=input$tab1.trans.wAY,
                             Abs=input$tab1.trans.wAA)))
    })
  tab1.measures<-reactive({
    errorprobs=tab1.errorprobs()
    Sample<-tab1.Sample()
    measures<-Sample|>plyr::ddply(~Gender+y,
                         function(d){data.frame(y=c('No','Yes','Abs'),count=rmultinom(n=1,size=d$sample,prob=errorprobs[[d$Gender]][[d$y]]))})|>
      dplyr::group_by(Gender,y)|>
      dplyr::summarise(count=sum(count))|>
      dplyr::mutate(y=y|>factor(levels=c('No','Yes','Abs'))|>ordered())

  })
  
  
  
  tab1.census<-reactive({
    Pop=tab1.Pop()
    Census<-Pop|>dplyr::group_by(Gender)|>
      dplyr::summarise(Count=sum(Count))|>
      dplyr::mutate(Estimate=
                      MASS::mvrnorm(1, mu = Count*(1+c(input$tab1.censusbias.w,input$tab1.censusbias.m)/100),
                                    Sigma = diag(Count*c(input$tab1.censusprec.w,input$tab1.censusprec.m)/100)%*%
                                      matrix(c(1,input$tab1.censuscorr/100,input$tab1.censuscorr/100,1),2,2)%*%
                                      diag(Count*c(input$tab1.censusprec.w,input$tab1.censusprec.f)/100))|>round())|>
      dplyr::select(Gender,Estimate)
  })
  
  tab1.data.final<-reactive({
    Pop=tab1.Pop()
    Census<-tab1.census()
    measures<-tab1.measures()
    Final=measures|>
      dplyr::group_by(Gender)|>
      dplyr::mutate(total=sum(count))|>
      dplyr::left_join(Census)|>
      dplyr::mutate(Estimate=round(count*Estimate/total))|>
      dplyr::select(Gender,y,Estimate)
  })
  
  # observeEvent(input$tab1.trans.mYY,  {
  #   updateSliderInput(session = session, inputId = "tab1.trans.mYN", value = max(0,min(1,1 - input$tab1.trans.mYY-input$tab1.trans.mYA)))
  #   updateSliderInput(session = session, inputId = "tab1.trans.mYA", value = max(0,min(1,1 - input$tab1.trans.mYY-input$tab1.trans.mYN)))})
  # 
  #   observeEvent(input$tab1.trans.mYN,  {
  #     updateSliderInput(session = session, inputId = "tab1.trans.mYA", value =max(0,min(1, 1 - input$tab1.trans.mYY-input$tab1.trans.mYN)))})
  # 
  #   observeEvent(input$tab1.trans.mYA,  
  #       updateSliderInput(session = session, inputId = "tab1.trans.mYY", value = max(0,min(1,1 - input$tab1.trans.mYA-input$tab1.trans.mYN))))
    

    output$tab1.plot1 <- renderPlot(
      { 
        
        ggplot(data=tab1.Pop(),
               aes(x=Gender,fill=y,y=Count,label=Count))+
          geom_bar(stat='identity',color='white')+xlab("")+ylab("")+
          geom_text(aes(label=Count), position=position_stack(vjust = 0.5), vjust=1.5)+
          scale_fill_manual('',values=c("red", "green", "white"))+theme(legend.position="bottom",                 axis.text.x = element_text(color = "white",angle=0))
        
        
      })
    
    
    
    
  output$tab1.plot6 <- renderPlot(
    { 
      Pop<-tab1.Pop()
      Final<-tab1.data.final()
      alltogether<-Pop|>dplyr::left_join(Final)|>
        reshape2:::melt.data.frame(id.vars = c("Gender","y"),variable.name = "type")
      
      ggplot(data=alltogether,
             aes(x=type,fill=y,y=value,label=value))+
        geom_bar(stat='identity',color='white')+
        xlab("")+
        ylab("")+
        geom_text(aes(label=value), position=position_stack(vjust = 0.5), vjust=1.5)+
        scale_fill_manual('',values=c("red", "green", "white"))+
        theme(legend.position="bottom",
              axis.text.x = element_text(color = "white",angle=0))
      
      
    })
    
    
    
    output$tab1.plot2 <- renderPlot(
      { 
        
        ggplot(data=tab1.Sample(),
               aes(x=Gender,fill=y,y=sample,label=sample))+
          geom_bar( stat='identity')+xlab("")+ylab("")+
          geom_text(aes(label=sample), position=position_stack(vjust = 0.5), vjust=1.5)+
          scale_fill_manual('',values=c("red", "green", "white"))+theme(legend.position="bottom",                 axis.text.x = element_text(color = "white",angle=0))
        
        
      }
  )      
  
    
    
    output$tab1.plot3 <- renderPlot(
      { 
        measures=tab1.measures()
        ggplot(data=measures,
               aes(x=Gender,fill=y,y=count,label=count))+
          geom_bar(stat='identity',color='white')+xlab("")+ylab("")+
          geom_text(aes(label=count), position=position_stack(vjust = 0.5))+
          scale_fill_manual('',values=c("red", "green", "white"))+theme(legend.position="bottom",                 axis.text.x = element_text(color = "white",angle=0))
        
        
      }
    )      
    
    
    output$tab1.plot4 <- renderPlot(
      { 
        Census<-tab1.census()
        ggplot(data=Census|>as.data.frame(),
               aes(x=Gender,fill=Gender,y=Estimate,label=Estimate))+
          geom_bar( stat='identity')+xlab("")+ylab("")+
          geom_text(position=position_stack(vjust = 0.5))+
          scale_fill_manual('',values=c("pink", "cyan"))+
          theme(legend.position="bottom",
                axis.text.x = element_text(color = "white",angle=0))
        }
    )      

    output$tab1.plot5 <- renderPlot(
      { 
        Final=tab1.data.final()
        ggplot(data=Final,
               aes(x=Gender,fill=y,y=Estimate,label=Estimate))+
          geom_bar(stat='identity',color='white')+xlab("")+ylab("")+
          geom_text(aes(label=Estimate), position=position_stack(vjust = 0.5), vjust=1.5)+
          scale_fill_manual('',values=c("red", "green", "white"))+theme(legend.position="bottom",                 axis.text.x = element_text(color = "white",angle=0))
      }
    )      
    
    
      
  # Panel 2
}


shinyApp(ui = ui, server = server)
