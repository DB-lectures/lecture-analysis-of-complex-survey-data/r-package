require(shiny)
require(shinythemes)
require(reshape2)
require(ggplot2)
require(plyr)
require(dplyr)
require(patchwork)

if(F){
input<-list(tab2.N=100,tab2.n=10,tab1.mu0=10,tab1.sigmamu=2,tab1.alpha=1,tab1.beta=1,tab1.sigma=1,tab1.king=F,tab1.plotoption='1',tab1.plotoption1='1',
            tab2.Y300=2,
            tab2.Z300=2,
            tab2.mu0=10,tab2.sigmamu=2,tab2.alpha=1,tab2.beta=1,tab2.sigma=1,tab2.king=F,
            tab2.r=.1
            )
}
n<-10
N<-100
#'@examples
#' Y<-Y.f(100,T,1,1)
Y.f<-function(N=100,king=T,mu0=1,sigmamu=1){
  Nh=rmultinom(n = 1,size = N,prob = rep(1/3,3))
  Y1<-rep(1:3,Nh)
  mu=rnorm(3,mean=mu0,sd=sigmamu)
  sds=rchisq(3,1)
  Y2<-rnorm(sum(Nh),mean=rep(mu,Nh),sd=rep(sds,Nh))
  if(king){Y2[N]<-sum(Y2)}
  list(Y=cbind(Y1,Y2),Nh=Nh)}
#'@examples
#' Y<-Y.f(100,T,1,1);Y
#' Z<-Z.f(Y,10,100,0,1,1,'Neyman');Z

Z.f<-function(Y,n=10,N=100,alpha=0,beta=1,sigma=1,typeofalloc){
  Z<-rnorm(sum(Y$Nh),mean=rnorm(alpha+beta*Y$Y[,2]),sd=sigma);
  Sh2<-sapply(1:3,function(i){sd(Y$Y[Y$Y[,1]==i,2])})
  nh=pmax(1,round(n*Y$Nh*Sh2/sum(Y$Nh*Sh2)))
  nh=pmin(nh,Y$Nh)
  pik<-rep(nh/Y$Nh,Y$Nh)
  list(n=n,N=N,nh=nh,Nh=Y$Nh,Z1=Y$Y[,1],Z=Z,pik=pik,w=1/pik)}
k.f<-function(Z=1,N=100,n=10){1:100}
#'@examples
#' Y<-Y.f(100,T,1,1)
#' Z<-Z.f(Y,10,100,0,1,1,'Neyman')
#' I<-I.f(Z);I
I.f    <-function(Z){
  list(srs=1*is.element(1:sum(Z$Nh),sample(sum(Z$N),sum(Z$n))),
         I=(sapply(1:3,function(i){sample((1:Z$N)[Z$Z1==i],Z$nh[i])})|>unlist()|>is.element(el=1:Z$N))*1)}

#'@examples
#' Y<-Y.f(100,T,1,1)
#' Z<-Z.f(Y,10,100,0,1,1,'Neyman')
#' I<-I.f(Z)
#' estimates.f(Y,Z,I)
estimates.f<-function(Y,Z,I){rbind(Census=rep(1,sum(Y$Nh)),Sample=I$I*Z$w,SRS=I$srs*sum(Y$Nh)/sum(Z$nh))%*%(Y$Y[,2])}

plot1.f<-function(PopPlot){
  ggplot(data=PopPlot|>dplyr::arrange(I),
         aes(x=Z,y=Y,size=size1,color=as.factor(I),group_by(Stratum)))+
    geom_point(alpha=.4)+ylab("Y")+xlab("Z")+
    theme(legend.position="bottom",axis.text.x = element_text(color = "white"))+
    scale_size_continuous(limits = c(0,max(PopPlot$max1)),range = c(0,min(20,max(PopPlot$max1))))+ 
    #scale_x_continuous(limits = c(0,max(PopI$PopI$k)*1.3)) + 
    #scale_y_continuous(limits = c(0,max(PopI$PopI$Y)*1.3))+
    theme(legend.position="none")+
#    geom_rug(size=.5,aes(color=as.factor(I)))+
    facet_grid(~Stratum)
}

plot2.f<-function(PopPlot){
  ggplot(data=PopPlot|>dplyr::filter(I==1),
         aes(x=Z,y=Y,size=size))+
    geom_point(alpha=.4,color='white')+ylab("Y")+xlab("Z")+
    theme(legend.position="bottom",axis.text.x = element_text(color = "white"))+
    scale_size_continuous(limits = c(0,max(PopPlot$max0)),range = c(0,min(20,max(PopPlot$max0))))+ 
    theme(legend.position="none")+
    geom_rug(size=.5,aes(color=as.factor(I)))+
    facet_grid(~Stratum)
}




ui <-function () {
  fluidPage( 
    tags$head(
      tags$link(rel="stylesheet", 
                href="https://cdn.jsdelivr.net/npm/katex@0.10.1/dist/katex.min.css", 
                integrity="sha384-dbVIfZGuN1Yq7/1Ocstc1lUEm+AT+/rCkibIcC/OmWo5f0EA48Vf8CytHzGrSwbQ",
                crossorigin="anonymous"),
      HTML('<script defer src="https://cdn.jsdelivr.net/npm/katex@0.10.1/dist/katex.min.js" integrity="sha384-2BKqo+exmr9su6dir+qCw08N2ZKRucY4PrGQPPWU1A7FtlCGjmEGFqXCv5nyM5Ij" crossorigin="anonymous"></script>'),
      HTML('<script defer src="https://cdn.jsdelivr.net/npm/katex@0.10.1/dist/contrib/auto-render.min.js" integrity="sha384-kWPLUVMOks5AQFrykwIup5lo0m3iMkkHrD0uJ4H5cjeGihAutqP0yW0J6dpFiVkI" crossorigin="anonymous"></script>'),
      HTML('
    <script>
      document.addEventListener("DOMContentLoaded", function(){
        renderMathInElement(document.body, {
          delimiters: [{left: "$", right: "$", display: false}]
        });
      })
    </script>')
    ),
    # Application title
    #setBackgroundColor("black"),
    navbarPage(
      "Analysis of Complex surveys",id="main_navbar",theme = shinytheme("slate"),
      tabPanel("One draw",
               fluidRow(column(3,
                               h3("Population parameters"),
                               p("Stratum: $P^{Y_1}=\\mathrm{Multinomial}(N,(1/3,1/3,1/3))$"),
                               p("Mean per stratum $\\mu_h\\sim\\mathrm{Normal}(\\mu_0,\\sigma_\\mu^2)$"),
                               p("Std. dev. per stratum $\\sigma_h\\sim\\mathrm{\\chi}^2(0,1)$"),
                               h4("$P^{Y_2|Y_1=h;\\sigma=\\sigma,\\mu=\\mu}=\\mathrm{Normal}(\\alpha_h,\\sigma_h^2)$"),
                               fluidRow(column(6,sliderInput('tab1.mu0',p('$\\mu_0$'),min=-3,max=20,step=1,value=5)),
                                        column(6,sliderInput('tab1.sigmamu',p('$\\sigma_{\\mu}$'),  min=0,max=10,step=1,value=5))),
                               checkboxInput('tab1.king',label = 'King effect',value = F),
                               h4("$P^{Z|Y}=\\mathrm{Normal}(\\alpha+\\beta Y,\\sigma^2_Z I_N)$"),
                               fluidRow(column(4,sliderInput('tab1.alpha',p('$\\alpha$'),min=0,max=20,step=1,value=5)),
                                        column(4,sliderInput('tab1.beta',p('$\\beta$'),  min=-3,max=10,step=1,value=5)),
                                        column(4,sliderInput('tab1.sigma',p('$\\sigma_Z$'),min=0,max=10,step=1,value=1))),
                               actionButton('newY','New $Y$, $Z$, $I$'),
                               actionButton('newZ','New $Z$, $I$'),
                               actionButton('newI','New $I$'),
                               selectInput(inputId = 'tab1.plotoption1',
                                           label = 'Pop plot options (Point size)',
                                           choices =c('$Y$'='Y',
                                                      '1'='1',
                                                      '$w$'='w',
                                                      '$Y\\times w$'="Y*w",
                                                      '$\\pi_k$'='pik')),
                               selectInput(inputId = 'tab1.plotoption',
                                           label = 'Sample plot options (Point size)',
                                           choices =c('$Y\\times w$'="Y*w",
                                                      '$w$'='w',
                                                      '1'='1',
                                                      '$Y$'='Y',
                                                      '$\\pi_k$'='pik')),
                               plotOutput("tab1.plot5",height = 400)),
                        column(9,
                               fluidRow(
                                 column(6,
                                        h3("Population"),
                                        plotOutput("tab1.plot1",height = 300)),
                                 column(6,
                                        h3("Population"),
                                        plotOutput("tab1.plot3",height = 300))),
                               fluidRow(
                                 column(6,
                                        h3("Sample"),
                                        plotOutput("tab1.plot2",height = 300)),
                                 column(6,
                                        h3("Sample"),
                                        plotOutput("tab1.plot4",height = 300))
                               )
                        )
               )
      ),
      tabPanel("Three hundred draws",
               fluidRow(column(3,
                               h3("Population parameters"),
                               p("Stratum: $P^{Y_1}=\\mathrm{Multinomial}(N,(1/3,1/3,1/3))$"),
                               p("Mean per stratum $\\mu_h\\sim\\mathrm{Normal}(\\mu_0,\\sigma_\\mu^2)$"),
                               p("Std. dev. per stratum $\\sigma_h\\sim\\mathrm{\\chi}^2(0,1)$"),
                               h4("$P^{Y_2|Y_1=h;\\sigma=\\sigma,\\mu=\\mu}=\\mathrm{Normal}(\\alpha_h,\\sigma_h^2)$"),
                               fluidRow(column(6,sliderInput('tab2.mu0',p('$\\mu_0$'),min=-3,max=20,step=1,value=5)),
                                        column(6,sliderInput('tab2.sigmamu',p('$\\sigma_{\\mu}$'),  min=0,max=10,step=1,value=5))),
                               checkboxInput('tab2.king',label = 'King effect',value = F),
                               h4("$P^{Z|Y}=\\mathrm{Normal}(\\alpha+\\beta Y,\\sigma^2_Z I_N)$"),
                               fluidRow(column(4,sliderInput('tab2.alpha',p('$\\alpha$'),min=0,max=20,step=1,value=5)),
                                        column(4,sliderInput('tab2.beta',p('$\\beta$'),  min=-3,max=10,step=1,value=5)),
                                        column(4,sliderInput('tab2.sigma',p('$\\sigma_Z$'),min=0,max=10,step=1,value=1))),
                               sliderInput('tab2.N',label = '$N$',min = 10,max=2010,step=50,value=110),
                               sliderInput('tab2.r',label = '$n/N$',min = .1,max=.9,step=.1,value=.1),
                               fluidRow(column(4,actionButton('tab2.newY','New $Y$, $Z$, $I$'),
                                               radioButtons('tab2.Y300',p('draw $Y$'),choices = c('1 time'=1,
                                                                                                   '300 times'=300))),
                                        column(4,actionButton('tab2.newZ','New $Z$, $I$'),
                                               radioButtons('tab2.Z300',p('draw $Z$'),choices = c('1 time'=1,'300 times'=300))),
                                        column(4,
                                        actionButton('tab2.newI','New $I$')))),
                        column(9,plotOutput("tab2.plot1",height = 600))))))}
server <- function(input , output,session) {
  library(dplyr)
  theme_dark2 = function() {
    theme_grey() %+replace%
      theme(
        # Specify axis options
        axis.line = element_blank(),  
        axis.text.x = element_text(color = "white"),  
        axis.text.y = element_text(colour = "white"),  
        axis.ticks = element_line(color = "white"),  
        axis.title.x = element_text(colour = "white"),  
        axis.title.y = element_text(colour = "white"),
        # Specify legend options
        legend.background = element_rect(color = NA, fill = " gray10"),  
        legend.key = element_rect(colour = "white",  fill = " gray10"),  
        legend.text = element_text(colour = "white"),  
        legend.title = element_text(colour = "white"),
        # Specify panel options
        panel.background = element_rect(fill = " gray10", color  =  NA),  
        panel.border = element_rect(fill = NA, color = "white"),  
        panel.grid.major = element_line(color = "grey35"),  
        panel.grid.minor = element_line(color = "grey20"),
        # Specify facetting options
        strip.background = element_rect(fill = "grey30", color = "grey10"),  
        strip.text.x = element_text(color = "white"),  
        strip.text.y = element_text(color = "white"),  
        # Specify plot options
        plot.background = element_rect(color = " gray10", fill = " gray10"),  
        plot.title = element_text(colour = "white"),  
        plot.subtitle = element_text(colour = "white"),  
        plot.caption = element_text( colour = "white")
      )}
  #ggplot(data=data.frame(x=1:15,z=1,y=factor(1:3)),aes(x=x,y=z,color=y))+geom_point()
  th<-theme_dark2()
  theme_set(th)
  my_palette <- c('lightblue', 'red', 'white','green')
  #names(my_palette)<-c('Original','Transformed',NA)
  #assign("scale_colour_discrete", function(..., values = my_palette) scale_colour_manual(..., values = values), globalenv())
  #assign("scale_fill_discrete", function(..., values = my_palette) scale_fill_manual(..., values = values), globalenv())
  #assign("scale_fill_ordinal", function(..., values = my_palette) scale_fill_manual(..., values = values), globalenv())
  #assign("scale_colour_ordinal", function(..., values = my_palette) scale_fill_manual(..., values = values), globalenv())
  #colScale <- scale_colour_manual(name = "Origin",values = c('Original'='lightblue', 'Transformed'='red','white'))
  #colScale2 <- scale_fill_manual(name = "Origin",values = c('Original'='lightblue', 'Transformed'='red','white'))
  
  #Risk with sampling with replacement approximation.
  
  
  
  ######################################## Tab1

  tab1.Pop<-eventReactive(c(1,input$newY,input$tab1.king),{
    Y<-Y.f(N=N,king=input$tab1.king,mu0=input$tab1.mu0,sigmamu = input$tab1.sigmamu)})
  
  
  tab1.PopZ<-eventReactive(c(1,input$newY,input$tab1.king,input$newZ),{
    Y<-tab1.Pop()
    Z<-Z.f(Y = Y,n = n)
        Pop<-list(PopY=data.frame(k=1:N)|>dplyr::mutate(
          Y=Y$Y[,2],
          pik=Z$pik,
          w=Z$w,Yw=w*Y,
          Z=Z$Z),Y=Y,Z=Z)
    })
  

  tab1.PopI<-eventReactive(c(1,input$tab1.king,input$newY,input$newI,input$tab1.alpha,input$tab1.beta,input$tab1.sigma),{
    Pop<-tab1.PopZ()
    Z<-Pop$Z
    Y<-Pop$Y
    PopY<-Pop$PopY
    I=I.f(Z = Z)
    PopI<-list(PopI=PopY|>
      dplyr::mutate(srs=I$srs,
                    I=I$I),Y=Y,I=I,Z=Z)
  })
  
  
    
  output$tab1.Pop.tbl<-renderTable({head(tab1.PopI()|>dplyr::select(-Yw),n=10)})
  output$tab1.Sample.tbl<-renderTable({head(tab1.PopPlot()|>dplyr::filter(I==1)|>dplyr::select(-size,-size1),n=nrow(tab1.Sample()))})
  
  
  
  tab1.PopPlot<-eventReactive(c(1,input$newY,input$newZ,input$newI,input$tab1.plotoption,input$tab1.plotoption1),{
    PopI<-tab1.PopI()
    PopPlot<-PopI$PopI|>
      dplyr::mutate(size=eval(parse(text = input$tab1.plotoption)),
                    size1=eval(parse(text = input$tab1.plotoption1)),
                    max1=max(size1),
                    max0=max(size),
                    Stratum=paste0('Stratum',PopI$Z$Z1))
    if(input$tab1.plotoption=='Yw'&input$tab1.plotoption1=='Y'){
      M1<-max(PopPlot$size1)
      M0<-max(PopPlot$size)
      m1<-min(20,M1)
      m0<-min(20,M0)
  
      PopPlot$max1<-m1
      PopPlot$max0<-m1
    }
    PopPlot
  })
  
   
    output$tab1.plot1 <- renderPlot(
      { 
        PopPlot<-tab1.PopPlot()
        plot1.f(PopPlot)
        })
    
    
    output$tab1.plot2 <- renderPlot(
      { PopPlot<-tab1.PopPlot()
        plot2.f(PopPlot)})
    
    output$tab1.plot3 <- renderPlot(
      { PopPlot<-tab1.PopPlot()
        qplot(PopPlot$Y,geom='histogram',color='Sample')+
          scale_x_continuous(limits = c(0,max(PopPlot$Y)*1.3))+theme(legend.position="none")+xlab("Y")
      })
    
    output$tab1.plot4 <- renderPlot(
      { PopPlot<-tab1.PopPlot()
        PopPlot2<-PopPlot|>dplyr::filter(I==1)
        qplot(PopPlot2$Y,weight=PopPlot2$w,geom='histogram',color='Sample')+
          scale_x_continuous(limits = c(0,max(PopPlot2$Y)*1.3))+theme(legend.position="none")+xlab("Y")
      })
    
    
    
    output$tab1.plot5 <- renderPlot(
      { 
        PopPlot<-tab1.PopPlot()
        XX=data.frame(Source=c("Population","Sample"),
                      Total=c(PopPlot[PopPlot$I==1,]$Yw|>sum(),PopPlot$Y|>sum()))
        ggplot(XX,aes(x=Source,y=Total,group=Source,color=Source,fill=Source))+geom_bar(stat='identity')+theme(legend.position="none")})
    
    
  
    
    
    
    
    
    
    
    
    
    
    
    
    
    ######################################## Tab2
    
    observeEvent(c(1,input$tab2.Y300),{
      if(input$tab2.Y300==300){
    updateRadioButtons(session,'tab2.Z300',choices=c('300 times'='300'))}  
      if(input$tab2.Y300==1){
      updateRadioButtons(session,'tab2.Z300',choices=c('1 time'='300','300 times'='300'))}
    })
    
    tab2.PopY<-eventReactive(c(1,input$newY,input$tab2.Y300,input$tab2.king,input$tab2.N),{
    PopY=plyr::rlply(strtoi(input$tab2.Y300),
                     Y.f(N=input$tab2.N,king=input$tab2.king,mu=input$tab2.mu0,sigmamu = input$tab2.sigmamu))
    })
    
    
    
    tab2.PopZ<-eventReactive(c(1,input$tab2.Y300,input$tab2.Z300,input$tab2.king,input$tab2.N,input$tab2.r,input$tab2.newY,input$tab2.newZ),{
      PopY<-tab2.PopY()
      PopZ<-PopY|>plyr::llply(function(Y){
          rlply(strtoi(input$tab2.Z300)/strtoi(input$tab2.Y300),
                list(Y=Y,Z=Z.f(Y,n=ceiling(input$tab2.N*input$tab2.r),N=input$tab2.N,beta=input$tab2.beta,alpha=input$tab2.alpha,sigma=input$tab2.sigma)))})|>
        do.call(what=c)
        })
    
    
    tab2.Totals<-eventReactive(c(1,input$tab2.Y300,input$tab2.Z300,input$tab2.king,input$tab2.r,input$tab2.newY,input$tab2.newZ,input$tab2.newI),{
      PopZ<-tab2.PopZ()
      Totals<-
        PopZ|>plyr::ldply(function(L){
          plyr::rdply(300/max(strtoi(input$tab2.Z300),strtoi(input$tab2.Y300)),
                      {Totals<-
                          t(estimates.f(Y = L$Y,Z=L$Z,I=I.f(Z = L$Z)))|>as.data.frame()})})
      })
    
    
    
    
    
    output$tab2.plot1 <- renderPlot(
      { 
        Totals<-tab2.Totals()
        plot1<-
        ggplot(data=Totals,
               aes(x=Census,y=Sample))+
          geom_vline(xintercept = Totals$Census|>mean(),colour='red',size=1)+
         # geom_vline(xintercept = Totals$Sample|>mean(),colour='red',size=2)+
          #geom_hline(yintercept = Totals$Census|>mean(),colour='blue',size=2)+
          geom_hline(yintercept = Totals$Sample|>mean(),colour='cyan',size=1)+
          geom_point(color='white')+
          geom_abline(slope=1,intercept=0,color='white')+
          ggtitle("HT estimators of the population total total")+
          xlim(c(0,max(Totals$Census,Totals$Sample)))+
          ylim(c(0,max(Totals$Census,Totals$Sample)))+
          geom_rug(color='white',size=.1)
        dens1<-ggplot(Totals,aes(x=Sample))+
          coord_flip()+
          xlim(c(0,max(Totals$Census,Totals$Sample)))+
          geom_histogram(aes(y=..density..),breaks=quantile(x = Totals$Sample,seq(0,1,length.out = 20)),color='white',fill='white',alpha=.5)+
          geom_density(alpha=.2,fill='white',color='white')+
          theme_void()+theme(legend.position = 'none')
        dens1SRS<-ggplot(Totals,aes(x=SRS))+
          coord_flip()+
          xlim(c(0,max(Totals$Census,Totals$Sample)))+
          geom_histogram(aes(y=..density..),breaks=quantile(x = Totals$SRS,seq(0,1,length.out = 20)),color='white',fill='white',alpha=.5)+
          geom_density(alpha=.2,fill='white',color='white')+
          theme_void()+theme(legend.position = 'none')
        dens2<-ggplot(Totals,aes(x=Sample))+
          geom_boxplot(color='white',fill='white',alpha=.5)+
          geom_vline(xintercept=Totals$Sample|>mean(),colour='cyan',size=1)+
          theme_void()+
          theme(legend.position = 'none')+
          xlim(c(0,max(Totals$Census,Totals$Sample)))+coord_flip()
        dens2SRS<-ggplot(Totals,aes(x=SRS))+
          geom_boxplot(color='white',fill='white',alpha=.5)+
          geom_vline(xintercept=Totals$SRS|>mean(),colour='green',size=1)+
          theme_void()+
          theme(legend.position = 'none')+
          xlim(c(0,max(Totals$Census,Totals$Sample)))+
          coord_flip()
       dens2SRS+dens2+plot1+dens1+dens1SRS+plot_layout(ncol=5,nrow=1,widths=c(1,1,4,1,1),heights=c(4))
      })
    
}


shinyApp(ui = ui, server = server)
