---
title: "Vignette Title"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Vignette Title}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---


```{r}  
CPSdat<-getCPSdata()
save(CPSdat,file.path(tempdir(),'CPSdat.rda'))#We save this file somewhere in order not to dowload it each time.
```



CPSdat|>View()
