Statistical Analysis of Complex Survey Data Using R
================

## Installation

To install the package, execute:

``` r
devtools::install_gitlab(repo = "DB-lectures/lecture-analysis-of-complex-survey-data/r-package",dep=T)
```
