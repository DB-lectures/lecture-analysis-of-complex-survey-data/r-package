#' Run a shiny app to compute optimal sample size
#' 
#' @examples 
#' LSurveySampling::run.app()
run.app.ch6.cube<-function(){
shiny::runApp(system.file("shinyapps/LecturesSurveySampling",package="LSurveySampling"), display.mode = "normal")
}
