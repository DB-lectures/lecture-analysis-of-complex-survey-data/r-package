#' Run a shiny app to compute optimal sample size
#' 
#' @examples 
#' LSurveySampling::run.app()
run.app<-function(){
shiny::runApp(system.file("shinyapps/LecturesSurveySampling",package="LSurveySampling"), display.mode = "normal")
}
