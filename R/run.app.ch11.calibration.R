#' Run a shiny app to compute optimal sample size
#' 
#' @examples 
#' LSurveySampling::run.app()
run.app.ch10.calibration<-function(){
shiny::runApp(system.file("shinyapps/ch11.pseudosamplemle",package="LSurveySampling"), display.mode = "normal")
}
