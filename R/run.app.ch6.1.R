#' Run a shiny app to compute optimal sample size
#' 
#' @examples 
#' LSurveySampling::run.app.ch6.1()
run.app.ch6.1<-function(){
shiny::runApp(system.file("shinyapps/ch6.1",package="LSurveySampling"), display.mode = "normal")
}
